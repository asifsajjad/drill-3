const data = require('./data.json');
const fs = require('fs');

const assignNewCVVField = require('./problem3');
const addCardValidField = require('./problem4');
const invalidateCardsBeforeMarch = require('./problem5');
const sortAsPerIssueDate = require('./problem6');
const groupByMonthName = require('./problem7');

function getOutput(data) {
    assignNewCVVField(data);
    addCardValidField(data);
    invalidateCardsBeforeMarch(data);
    sortAsPerIssueDate(data);
    groupByMonthName(data);
    // console.log(data);
    fs.writeFile(`${__dirname}/output.json`, JSON.stringify(data), (err) => { if (err) console.log(err) });
}

getOutput(data);

