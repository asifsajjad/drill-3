// 2. Find all cards that were issued before June.

const data = require('./data.json');

function findBeforeJuneCards(data) {
    return data.filter((cardDetails) => {
        let cardIssueMonth = cardDetails.issue_date.split('/')[0];
        // console.log(cardIssueMonth);
        return (parseInt(cardIssueMonth) < 6);
    });
}

module.exports = findBeforeJuneCards;
// console.log(findBeforeJuneCards(data));