// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.

const data = require('./data.json');

function assignNewCVVField(data) {
    return data.map((cardDetails) => {
        cardDetails["CVV"] = Math.floor(Math.random() * 899) + 100;
        return cardDetails;
    });
}

module.exports = assignNewCVVField
// console.log(assignNewCVVField(data));