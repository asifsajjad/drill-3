// 5. Invalidate all cards issued before March.

const data = require('./data.json');

function invalidateCardsBeforeMarch(data) {
    return data.map((cardDetails) => {
        let cardIssueMonth = cardDetails.issue_date.split('/')[0];
        if (parseInt(cardIssueMonth) < 3) {
            cardDetails.valid = false;
        }
        return cardDetails;
    });
}

module.exports = invalidateCardsBeforeMarch;