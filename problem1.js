// 1. Find all card numbers whose sum of all the even position digits is odd.

const data = require('./data.json');

function isEvenPosSumOdd(userString, sum) {
    // console.log(userString);
    if (userString.length <= 1) {
        return sum % 2 === 1;
    }
    // console.log(sum);
    sum = sum + parseInt(userString.charAt(1));
    return isEvenPosSumOdd(userString.substring(2), sum);
}

function findEvenPositionSumOddCardNum(data) {
    return data.filter((cardDetails) => {
        let cardNum = cardDetails.card_number;
        return isEvenPosSumOdd(cardNum, 0);
    });

}
console.log(findEvenPositionSumOddCardNum(data));
