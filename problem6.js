// 6. Sort the data into ascending order of issue date.

const data = require('./data.json');

function sortAsPerIssueDate(data) {
    return data.sort((cardDetailA, cardDetailB) => {
        let dateA = new Date(cardDetailA.issue_date);
        let dateB = new Date(cardDetailB.issue_date);
        return dateA - dateB;
    });
}

module.exports = sortAsPerIssueDate;

// console.log(sortAsPerIssueDate(data));