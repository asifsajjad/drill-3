// 7. Group the data in such a way that we can identify which cards were assigned in which months.

const data = require('./data.json');

function groupByMonthName(sortedData) {
    return sortedData.map((cardDetails) => {
        let monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let cardIssueMonth = cardDetails.issue_date.split('/')[0];
        cardDetails["month"] = monthName[cardIssueMonth - 1];
        return cardDetails;
    });
}

module.exports = groupByMonthName;
// console.log(groupByMonthName(data));