// 4. Add a new field to each card to indicate if the card is valid or not.

const data = require('./data.json');

function addCardValidField(data) {
    return data.map((cardDetails) => {
        cardDetails["valid"] = '';
        return cardDetails;
    });
}

module.exports = addCardValidField;
// console.log(addCardValidField(data));